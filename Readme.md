Requiremenmts:
1) .net core 3.0 SDK
2) Visual Studio 2019

Build and Execution:

1) Open HabrLoader.sln solution in the Visual Studio
2) Open Solution explorer, Right Click on HabrLoader project and then Rebuild

3) Go to \HabrLoader\bin\Debug\netcoreapp3.0 directory
4) Execute commands in the following order:
    HabrLoader.exe FindActivePosts
    HabrLoader.exe DownloadPosts
    HabrLoader.exe CreateDataSet

5) Step 4 generates file dataset.csv
6) Return back to Visual Studio, open Solution explorer, Right click on HabrAnalysis and then Rebuild
7) Go to \HabrAnalysis\bin\Debug\netcoreapp3.0 directory
8) Execute HabrAnalysis.exe to open application with analysis and plots