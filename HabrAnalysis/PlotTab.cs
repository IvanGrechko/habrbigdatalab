﻿using System.Windows.Forms;
using OxyPlot;
using OxyPlot.WindowsForms;

namespace HabrAnalysis
{
    public partial class PlotTab : TabPage
    {
        private readonly PlotView plotView_;
        public PlotTab()
        {
            InitializeComponent();

            plotView_ = new PlotView();
            plotView_.Model = new OxyPlot.PlotModel();
            plotView_.Dock = DockStyle.Fill;

            this.Controls.Add(plotView_);
        }

        public void Populate(PlotModel model)
        {
            this.Text = model.Title;
            plotView_.Model = model;
            plotView_.InvalidatePlot(true);
        }
    }
}
