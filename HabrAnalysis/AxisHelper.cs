﻿using OxyPlot;
using OxyPlot.Axes;
using System.Collections.Generic;

namespace HabrAnalysis
{
    public static class AxisHelper
    {
        public static DateTimeAxis CreateDateAxis(AxisPosition position)
        {
            return new DateTimeAxis()
            {
                Title = "Date",
                StringFormat = "MM-dd",
                Position = position,
                IntervalLength = 40,
                MinorIntervalType = DateTimeIntervalType.Days,
                IntervalType = DateTimeIntervalType.Days,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
            };
        }

        public static DateTimeAxis CreateTimeAxis(AxisPosition position)
        {
            return new DateTimeAxis()
            {
                Title = "Time",
                StringFormat = "H:mm:ss",
                Position = position,
                MinorIntervalType = DateTimeIntervalType.Hours,
                IntervalType = DateTimeIntervalType.Hours,
                IntervalLength = 30,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.None,
            };
        }

        public static LinearAxis CreateLinearAxis(AxisPosition position, string title)
        {
            return new LinearAxis()
            {
                Title = title,
                AxislineStyle = LineStyle.None,
                Position = position,
                MinorTickSize = 0,
                MajorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Minimum = 0,
            };
        }

        public static LinearAxis CreateCategoryAxis(AxisPosition position, string title, IEnumerable<string> categories)
        {
            var axis = new CategoryAxis()
            {
                Title = title,
                AxislineStyle = LineStyle.None,
                Position = position,
                MinorTickSize = 0,
                MajorTickSize = 0,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Minimum = 0,
                Angle = 90
            };

            foreach(var category in categories)
            {
                axis.ActualLabels.Add(category);
            }

            return axis;
        }
    }
}
