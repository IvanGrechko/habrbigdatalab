﻿using HabrLoader;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace HabrAnalysis
{
    public partial class MainForm : Form
    {
        private readonly TabControl tabControl_;
        public MainForm()
        {
            InitializeComponent();

            tabControl_ = new TabControl();
            this.Controls.Add(tabControl_);
            tabControl_.Dock = DockStyle.Fill;

            InititalizeTabs();
        }

        private void InititalizeTabs()
        {
            var articles = CsvContainer.Load("dataset.csv");

            foreach (var plot in GetPlots(articles))
            {
                var plotTab = new PlotTab();
                plotTab.Populate(plot);

                tabControl_.TabPages.Add(plotTab);
            }
        }

        private IEnumerable<PlotModel> GetPlots(IList<ArticleModel> articles)
        {
            yield return GetDateDistribution(articles);
            yield return GetTimeDistribution(articles);
            yield return GetDayDistribution(articles);
            yield return GetTopTagViewDistribution(articles, 40);
            yield return GetBottomTagViewDistribution(articles, 40);
            yield return GetUserPostingDistribution(articles, 1);
        }

        private PlotModel GetDateDistribution(IList<ArticleModel> articles)
        {
            var model = new PlotModel();
            model.Title = "Date Distribution";

            var dateGroups = articles.Where(p => p.PublishTime != null).GroupBy(a => a.PublishTime.Value.Date)
                .OrderBy(g => g.Key)
                .ToList();

            model.Axes.Add(AxisHelper.CreateDateAxis(AxisPosition.Bottom));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Articles count"));

            var series = new FunctionSeries();
            foreach (var group in dateGroups)
            {  
                series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(group.Key), group.Count()));   
            }
            model.Series.Add(series);

            return model;
        }

        private PlotModel GetTimeDistribution(IList<ArticleModel> articles)
        {
            var model = new PlotModel();
            model.Title = "Time Distribution";

            var dateGroups = articles.Where(p => p.PublishTime != null).GroupBy(a => a.PublishTime.Value.TimeOfDay)
                .OrderBy(g => g.Key)
                .ToList();

            model.Axes.Add(AxisHelper.CreateTimeAxis(AxisPosition.Bottom));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Articles count"));

            var series = new FunctionSeries();
            foreach (var group in dateGroups)
            {
                series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(new DateTime(group.Key.Ticks)), group.Count()));
            }
            model.Series.Add(series);

            return model;
        }

        private PlotModel GetDayDistribution(IList<ArticleModel> articles)
        {
            var model = new PlotModel();
            model.Title = "Day Distribution";

            var dateGroups = articles.Where(p => p.PublishTime != null)
               .GroupBy(a => a.PublishTime.Value.DayOfWeek)
               .OrderBy(g => g.Key)
               .ToList();

            model.Axes.Add(AxisHelper.CreateCategoryAxis(AxisPosition.Bottom, "Days", dateGroups.Select(g => g.Key.ToString())));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Articles count"));

            var series = new ColumnSeries();
            for (int groupIndex = 0; groupIndex < dateGroups.Count; groupIndex++)
            {
                var group = dateGroups[groupIndex];
                series.Items.Add(new ColumnItem(group.Count(), groupIndex));
            }
            model.Series.Add(series);

            return model;
        }

        private PlotModel GetTopTagViewDistribution(IList<ArticleModel> articles, int topCount)
        {
            var model = new PlotModel();
            model.Title = $"Tag View Distribution. Top {topCount}";

            var dateGroups = articles.Where(p => p.PublishTime != null)
                .SelectMany(a => a.Tags.Select(t => new { Tag = t, Article = a }))
                .GroupBy(a => a.Tag, StringComparer.OrdinalIgnoreCase)
                .Select(a => new { Tag = a.Key, Views = a.Sum(g => g.Article.ViewCount.GetValueOrDefault(0)) / a.Count()})
                .OrderByDescending(g => g.Views)
                .Take(topCount)
                .ToList();

            model.Axes.Add(AxisHelper.CreateCategoryAxis(AxisPosition.Bottom, "Tags", dateGroups.Select(g => g.Tag)));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Views count"));

            var series = new ColumnSeries();
            for (int groupIndex = 0; groupIndex < dateGroups.Count; groupIndex++)
            {
                var group = dateGroups[groupIndex];
                series.Items.Add(new ColumnItem(group.Views, groupIndex));
            }
            model.Series.Add(series);

            return model;
        }

        private PlotModel GetBottomTagViewDistribution(IList<ArticleModel> articles, int bottomCount)
        {
            var model = new PlotModel();
            model.Title = $"Tag View Distribution. Bottom {bottomCount}";

            var dateGroups = articles.Where(p => p.PublishTime != null)
                .SelectMany(a => a.Tags.Select(t => new { Tag = t, Article = a }))
                .GroupBy(a => a.Tag, StringComparer.OrdinalIgnoreCase)
                .Select(a => new { Tag = a.Key, Views = a.Sum(g => g.Article.ViewCount.GetValueOrDefault(0)) / a.Count() })
                .OrderBy(g => g.Views)
                .Take(bottomCount)
                .ToList();

            model.Axes.Add(AxisHelper.CreateCategoryAxis(AxisPosition.Bottom, "Tags", dateGroups.Select(g => g.Tag)));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Views count"));

            var series = new ColumnSeries();
            for (int groupIndex = 0; groupIndex < dateGroups.Count; groupIndex++)
            {
                var group = dateGroups[groupIndex];
                series.Items.Add(new ColumnItem(group.Views, groupIndex));
            }
            model.Series.Add(series);

            return model;
        }

        private PlotModel GetUserPostingDistribution(IList<ArticleModel> articles, int topCount)
        {
            var model = new PlotModel();
            model.Title = $"User publishing Distribution. Top {topCount}";

            var userGroups = articles.Where(p => p.PublishTime != null)
                .GroupBy(a => a.GetAuthorName())
                .OrderByDescending(g => g.Count())
                .Take(topCount)
                .ToList();

            model.Axes.Add(AxisHelper.CreateDateAxis(AxisPosition.Bottom));
            model.Axes.Add(AxisHelper.CreateLinearAxis(AxisPosition.Left, "Articles count"));

            foreach (var usersGroup in userGroups)
            {
                var series = new FunctionSeries();
                series.Title = usersGroup.Key;
                foreach (var group in usersGroup.GroupBy(g => g.PublishTime.Value.Date).OrderBy(g => g.Key))
                {
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(group.Key), group.Count()));
                }
                model.Series.Add(series);
            }

            return model;
        }
    }
}
