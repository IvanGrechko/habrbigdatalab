﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace HabrLoader
{
    class Program
    {
        //private const string AllUrl = @"https://habr.com/ru/all/";
        private const string AllPageUrlTemplate = @"https://habr.com/ru/all/page{0}";
        private const string PostUrlTemplate = @"https://habr.com/ru/post/{0}/";
        private const string postsFolder = "Posts";
        private const string postsListFile = "all_posts.txt";
        private static readonly Regex PostUrlRegex = new Regex(@"https://habr\.com/ru/post/([^/]+)/");
        private static readonly HttpClient client_ = new HttpClient() { Timeout = new TimeSpan(0, 1, 0) };
        static async Task Main(string[] args)
        {
            var action = args[0];

            switch (action)
            {
                case "FindActivePosts":
                    {
                        await DownloadPostIdsAsync(postsListFile);
                        break;
                    }
                case "DownloadPosts":
                    {
                        var postIds = GetPostIds(postsListFile).Result;
                        await DownloadPostsAsync(postIds);
                        break;
                    }
                case "CreateDataSet":
                    {
                        var files = Directory.GetFiles(postsFolder);

                        await CreateDataSet(files);
                        break;
                    }
            }
        }

        private static async Task CreateDataSet(string[] articles)
        {
            var tasks = new List<Task<ArticleModel>>();
            int count = 0;
            foreach(var articePath in articles)
            {
                var task = Task<ArticleModel>.Run(() => ProcessArticle(articePath))
                    .ContinueWith<ArticleModel>(task =>
                    {
                        var model = task.Result;
                        Interlocked.Increment(ref count);
                        if (count % 100 == 0)
                        {
                            Console.WriteLine($"{count}\\{articles.Length}");
                        }
                        return model;
                    });
                tasks.Add(task);
            }

            var waitTask = Task.WhenAll<ArticleModel>(tasks);

            var articleModels = await waitTask;
            CsvContainer.Save(articleModels, "dataset.csv");
        }

        private static ArticleModel ProcessArticle(string path)
        {
            using (var file = new StreamReader(path))
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.Load(file);

                var articleModel = new ArticleModel();
                articleModel.Id = Path.GetFileNameWithoutExtension(path);
                PopulateModel(htmlDoc, articleModel);

                return articleModel;
            }
        }

        private static void PopulateModel(HtmlDocument htmlDoc, ArticleModel model)
        {
            var navigator = htmlDoc.DocumentNode.CreateNavigator();
            var isDeletedNode = navigator.SelectSingleNode($"//text()[contains(. , 'Доступ к публикации закрыт')]");
            var isdelted = isDeletedNode != null;
            model.IsDeleted = isdelted;
            if (isdelted)
            {
                var authorNode = navigator.SelectSingleNode("//p/a[contains(@href, 'https://habr.com/ru/users/')]");
                model.AuthorName = authorNode?.Value;
                model.AuthorLink = authorNode?.GetAttribute("href", null);

            }
            else
            {
                var postTimeNode = navigator.SelectSingleNode($"//span[@data-time_published]|//li[@data-time_published]");
                if (postTimeNode != null && DateTime.TryParse(postTimeNode.GetAttribute("data-time_published", null), out var postTime))
                {
                    model.PublishTime = postTime;
                }
                else
                {
                    Console.WriteLine($"Can not find DateTime for {model.Id}.");
                }

                var authorNode = navigator.SelectSingleNode($"//a[contains(@class, 'post__user-info user-info')]");
                var viewNode = navigator.SelectSingleNode("//span[contains(@class, 'post-stats__views-count')]");
                    //"|//li[contains(text(), 'просмотров'");
                var bookmarkNode = navigator.SelectSingleNode("//span[contains(@class, 'bookmark__counter js-favs_count')]");
                var commentNode = navigator.SelectSingleNode("//span[contains(@class, 'post-stats__comments-count')]");
                //voting-wjt__counter voting-wjt__counter_positive js-score
                var ratingNode = navigator.SelectSingleNode("//span[contains(@class, 'post-info__meta-counter')]");
                var titleNode = navigator.SelectSingleNode("//span[contains(@class, 'post__title-text')]");
                var habsGlobalNode = navigator.SelectSingleNode("//ul[contains(@class, 'post__hubs post__hubs_full-post inline-list')]");

                var habNodes = habsGlobalNode?.Select("li/a");
                var habs = habNodes?.OfType<XPathNavigator>().Select(v => v.Value).ToArray();

                model.AuthorLink = authorNode?.GetAttribute("href", null);
                model.AuthorName = authorNode?.SelectSingleNode("span")?.Value;
                model.BookmarkCount = NodeToInt(bookmarkNode);
                model.CommentCount = NodeToInt(commentNode);
                model.Rating = NodeToInt(ratingNode);
                model.Tags = habs;
                model.Title = titleNode?.Value;
                model.ViewCount = NodeToInt(viewNode);
            }
        }

        private static int? NodeToInt(XPathNavigator navigator)
        {
            if (navigator == null) return null;
            var value = navigator.Value.ToLowerInvariant().Replace(" просмотров", "")
                .Replace("&plus;", "+")
                .Replace("&minus;", "+")
                .Replace(",", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);

            int multiplayer = 1;
            if (value.EndsWith("k"))
            {
                multiplayer = 1000;
                value = value.Replace("k", "");
            }
            return (int)(double.Parse(value) * multiplayer);
        }

        private static async Task DownloadPostsAsync(string[] postIds)
        {
            var allFiles = Directory.GetFiles(postsFolder, "*.html", SearchOption.TopDirectoryOnly);
            var existingPosts = allFiles.Select(f => Path.GetFileNameWithoutExtension(f)).ToHashSet();

            postIds = postIds.Where(p => !existingPosts.Contains(p)).ToArray();

            foreach (var post in postIds)
            {
                var postUrl = string.Format(PostUrlTemplate, post);
                var doc = await LoadDocument(postUrl);
                using (var file = new StreamWriter($"{postsFolder}\\{post}.html"))
                {
                    doc.Save(file);
                }
            }
        }

        private static async Task<string[]> GetPostIds(string targetPath)
        {
            using (var file = new StreamReader(targetPath))
            {
                var hashSet = new HashSet<string>();
                while (!file.EndOfStream)
                {
                    var postId = await file.ReadLineAsync();
                    if (!string.IsNullOrWhiteSpace(postId))
                    {
                        hashSet.Add(postId);
                    }
                }

                return hashSet.ToArray();
            }
        }

        private static async Task DownloadPostIdsAsync(string targetPath)
        {
            using (var file = File.OpenWrite(targetPath))
            using (var writer = new StreamWriter(file))
            {
                const int maxSize = 470003;

                int currentMonth = 12;
                for (int postId = maxSize; postId >= 0; postId--)
                {
                    var pageUrl = string.Format(PostUrlTemplate, postId);
                    var htmlDoc = await LoadDocument(pageUrl);
                    if (Exist(htmlDoc, out var year, out var month, out var day))
                    {
                        if (month > 0 && currentMonth > month)
                        {
                            currentMonth = month;
                            Console.WriteLine("Processing month: {0}", currentMonth);
                        }
                        await writer.WriteLineAsync(postId.ToString());
                    }

                    if (postId % 1000 == 0)
                    {
                        Console.WriteLine($"{maxSize - postId + 1}\\{maxSize}");
                        await Task.Delay(new TimeSpan(0, 1, 0));
                    }
                }
            }
        }

        private static bool Exist(HtmlAgilityPack.HtmlDocument htmlDoc, out int year, out int month, out int day)
        {
            var pathNavigator = htmlDoc.DocumentNode.CreateNavigator().SelectSingleNode($"//text()[contains(., 'Страница не найдена')]/..");

            var exist = pathNavigator == null;
            year = -1;
            month = -1;
            day = -1;
            if (exist)
            {
                var postTimeNode = htmlDoc.DocumentNode.CreateNavigator().SelectSingleNode($"//span[contains(@class, 'post__time')]");
                if (postTimeNode != null)
                {
                    var postTimeValue = postTimeNode.GetAttribute("data-time_published", null);
                    if (postTimeValue != null && DateTime.TryParse(postTimeValue, out var time))
                    {
                        year = time.Year;
                        month = time.Month;
                        day = time.Day;
                    }
                }
            }
            return exist;
        }

        private static async Task<HtmlAgilityPack.HtmlDocument> LoadDocument(string url)
        {
            for (int index = 0; index < 3; index++)
            {
                try
                {
                    var content = await (await client_.GetAsync(url)).Content.ReadAsStringAsync();
                    var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                    htmlDoc.LoadHtml(content);

                    return htmlDoc;
                }
                catch (Exception e)
                {
                    await Task.Delay(new TimeSpan(0, 5, 0));
                }
            }

            throw new Exception("Can not load page.");
        }
    }
}
