﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HabrLoader
{
    public static class CsvContainer
    {
        public static void Save(IList<ArticleModel> models, string path)
        {
            Configuration configuration = CreateConfiguration();

            using (var writer = new StreamWriter(path))
            using (var csv = new CsvHelper.CsvWriter(writer, configuration, true))
            {
                csv.WriteRecords(models);
            }
        }

        private static Configuration CreateConfiguration()
        {
            var configuration = new Configuration();
            configuration.Delimiter = ";";
            configuration.HasHeaderRecord = true;
            configuration.TypeConverterCache.AddConverter<string[]>(new TypeConverter());
            configuration.Encoding = Encoding.UTF8;
            return configuration;
        }

        public static IList<ArticleModel> Load(string path)
        {
            Configuration configuration = CreateConfiguration();

            using (var reader = new StreamReader(path))
            using (var csv = new CsvHelper.CsvReader(reader, configuration, true))
            {
                return csv.GetRecords<ArticleModel>().ToList(); ;
            }
        }

        private class TypeConverter : ITypeConverter
        {
            public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
            {
                return text.Split(",");
            }

            public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
            {
                var array = (string[])value;
                if (array == null) return string.Empty;

                return string.Join(",", array);
            }
        }
    }
}
