﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HabrLoader
{
    public class ArticleModel
    {
        public bool IsDeleted { get; set; }

        public string Id { get; set; }

        public string Title { get; set; }

        public string AuthorName { get; set; }

        public string AuthorLink { get; set; }

        public DateTime? PublishTime { get; set; }

        public int? ViewCount { get; set; }

        public int? CommentCount { get; set; }

        public int? BookmarkCount { get; set; }

        public int? Rating { get; set; }

        public string[] Tags { get; set; }

        public string GetAuthorName()
        {
            if (string.IsNullOrWhiteSpace(AuthorName))
            {
                return AuthorLink.Replace(@"https://habr.com/ru/users/", "").Trim('/');
            }
            return AuthorName;
        }
    }
}
